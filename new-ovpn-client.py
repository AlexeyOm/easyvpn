#!/usr/bin/env python3

import os
import sys
import argparse
import datetime

#Define environments variables

#Put path to vars file here
os.environ["EASY_RSA_PATH"] = '/etc/openvpn/easy-rsa/2.0'
os.environ["OPENVPN_BACKUP_PATH"] = '/tmp/openvpn_{}'.format(str(datetime.date.today()))
os.environ["OPENVPN_BACKUP_OWNER"] = 'user:group'

def main(args):
	os.system('clear')
	os.system('. ' + os.environ['EASY_RSA_PATH'] + '/vars > /dev/null')

	#If cert exist exit
	for file in os.listdir(os.environ['KEY_DIR']):
		if args.certname == file.split('.')[0]:
			sys.exit('User ' + args.certname + ' exist!')
	os.system(os.environ['EASY_RSA'] + '/build-key --batch ' + args.certname)

	#pfx key export path
	pfx_key = os.environ['KEY_DIR'] + '/' + args.certname + '.pfx'

	#Absolute paths to needed files
	current_key = os.environ['KEY_DIR'] + '/' + args.certname + '.key'
	current_cert = os.environ['KEY_DIR'] + '/' + args.certname + '.crt'
	ca_crt = os.environ['KEY_DIR'] + '/ca.crt'

	#Create .pfx cert
	os.system(
		'openssl pkcs12 -export' + 
		' -out ' + pfx_key +
		' -inkey ' + current_key +
		' -in ' + current_cert +
		' -certfile ' + ca_crt +
		' -passout pass:' + args.password)

	print('Key was created')
	print('\n')	

	answer = input('Do you want to backup openvpn directory? yes\\no: ')

	if answer == 'yes':
		os.system('cp -R ' + os.environ['EASY_RSA'] + ' ' + os.environ["OPENVPN_BACKUP_PATH"])
		os.system('chown -R ' + os.environ["OPENVPN_BACKUP_OWNER"] + ' ' + os.environ["OPENVPN_BACKUP_PATH"])
	else:
		print("You choose not to make backup")

	print("All tasks was succesfuly done")

if __name__ == "__main__":

	#Define arguments
	parser = argparse.ArgumentParser()
	parser.add_argument("certname", help="Desired certname")
	parser.add_argument("password", help="Password for .pfx")

	#Paste arguments to main
	args = parser.parse_args()
	
	main(args)